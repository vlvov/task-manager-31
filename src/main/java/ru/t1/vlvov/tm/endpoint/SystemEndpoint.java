package ru.t1.vlvov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.api.service.IServiceLocator;
import ru.t1.vlvov.tm.dto.Request.ServerAboutRequest;
import ru.t1.vlvov.tm.dto.Request.ServerVersionRequest;
import ru.t1.vlvov.tm.dto.Response.ServerAboutResponse;
import ru.t1.vlvov.tm.dto.Response.ServerVersionResponse;

public final class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    private final IServiceLocator serviceLocator;

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setName(propertyService.getAuthorName());
        response.setEmail(propertyService.getAuthorEmail());
        return response;
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }
}
