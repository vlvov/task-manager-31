package ru.t1.vlvov.tm.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.component.Server;
import ru.t1.vlvov.tm.dto.Request.AbstractRequest;
import ru.t1.vlvov.tm.dto.Response.AbstractResponse;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public final class ServerRequestTask extends AbstractServerSocketTask {

    public ServerRequestTask(final Server server, final Socket socket) {
        super(server, socket);
    }

    @SneakyThrows
    @Override
    public void run() {
        @NotNull final InputStream inputStream = socket.getInputStream();
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final AbstractRequest request = (AbstractRequest) objectInputStream.readObject();
        @NotNull final AbstractResponse response = server.call(request);
        @NotNull final OutputStream outputStream = socket.getOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(response);
        server.submit(new ServerRequestTask(server, socket));
    }
}
