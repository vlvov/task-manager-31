package ru.t1.vlvov.tm.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.component.Server;

import java.net.ServerSocket;
import java.net.Socket;

public final class ServerAcceptTask extends AbstractServerTask {

    public ServerAcceptTask(@NotNull final Server server) {
        super(server);
    }

    @SneakyThrows
    @Override
    public void run() {
        @NotNull final ServerSocket serverSocket = server.getServerSocket();
        @NotNull final Socket socket = serverSocket.accept();
        server.submit(new ServerRequestTask(server, socket));
        server.submit(new ServerAcceptTask(server));
    }

}
