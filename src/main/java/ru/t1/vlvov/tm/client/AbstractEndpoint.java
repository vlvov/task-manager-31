package ru.t1.vlvov.tm.client;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
public abstract class AbstractEndpoint {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 2211;

    @NotNull
    private Socket socket;

    public AbstractEndpoint() {
    }

    public AbstractEndpoint(@NotNull String host, @NotNull Integer port) {
        this.host = host;
        this.port = port;
    }

    @NotNull
    private OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }

    @NotNull
    private InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    @NotNull
    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    @NotNull
    protected Object call(@NotNull final Object object) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(object);
        return getObjectInputStream().readObject();
    }

    protected void connect() throws IOException {
        socket = new Socket(host, port);
    }

    protected void disconnect() throws IOException {
        if (socket != null) socket.close();
    }

}
